@extends('template.main')

@section('title', 'Home')

@section('content')

<header class="main-header">
    <img src="{{ secure_asset('img/bg1.jpg') }}" alt="LAYEREDEFENSE" class="responsive-img hide-on-small-only animated" id="dynamic-img">
    <div class="valign-wrapper home-header bg1">
        <img src="{{ secure_asset('img/logo.png') }}" alt="Layeredense" class="responsive-img logo animated visible">
        <ul id="links-header">
            <li>
                <a menu-id="menu-scroll-0" href="#!" class="visible">
                    <img src="{{ secure_asset('img/icon/s1.png') }}" alt="About" class="animated">
                    About
                </a>
            </li><!--
            --><li>
                <a menu-id="menu-scroll-1" href="#!" class="visible">
                    <img src="{{ secure_asset('img/icon/s2.png') }}" alt="Our services" class="animated">
                    Our services
                </a>
            </li><!--
            --><li>
                <a menu-id="menu-scroll-2" href="#!" class="visible">
                    <img src="{{ secure_asset('img/icon/s3.png') }}" alt="Contact" class="animated">
                    Contact
                </a>
            </li><!--
            --><li>
                <a menu-id="menu-scroll-3" href="#!" class="visible">
                    <img src="{{ secure_asset('img/icon/s4.png') }}" alt="TheHackerGround" class="animated">
                    TheHackerGround
                </a>
            </li>
        </ul>
        <h1>Secure <span>IT Solutions</span></h1>
    </div>
    <div class="menu-bg">
        <span id="bg1" class="active"></span>
        <span id="bg2"></span>
        <span id="bg3"></span>
    </div>
</header>

<section class="what-we-do">    
    <div class="row top menu-scroll-0">
        <div class="col s12 m10 l10 offset-l1 xl10 offset-xl1">
            <h2>What we do</h2>
            <p class="text-g home">
                At LayereDefense, we champion that effective information security adds value to organizations by reducing losses from security-related incidents and providing assurance that security breaches are minimized and limited in scope. <br>
                <a href="{{ secure_url('about') }}" class="rm">Read More</a>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col s12 bg-wwd jarallax" id="particles-js" data-jarallax data-speed="0.7" data-imgSize="100% auto" data-type="scroll" style="background-image: url('{{ secure_asset('img/servers.jpg') }}');"></div>
    </div>
    <div class="row bg-gray">
        <div class="col s12 m10 l10 offset-l1 xl10 offset-xl1 bg-text">
            <p class="text-g">
                Flexible services designed around your business. <br>
                <a href="{{ secure_url('services') }}" class="rm">Read More</a>
            </p>
        </div>
        <div class="col s12 bg-wwd-2 menu-scroll-1"></div>
    </div>
</section>

<section class="art">
    <h2 class="hide">Technology, like Art, is a soraing exercise</h2>
    <div class="valign-wrapper">
        <span class="shape ny">Technology,</span>
        <div>
            <div>
                <span class="helv">like</span>
                <span class="otentic">Art,</span>
                <span class="helvBold">is a SORAING exercise</span>
            </div>
        </div>
        <span class="shape helv">of the human imagination...</span>
    </div>    
</section>

<section class="section section-contact center-align menu-scroll-2" id="layeredefense-contact">
    <div class="container">
        <div class="row">
            <div class="col s12 left-align">
                <h2>Contact us</h2>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m4 l3 xl3 address-cont left-align">
                <p>Tell us how we can help you!</p>                

                <div class="address" itemscope itemtype="http://schema.org/LocalBusiness">
                    <div itemprop="image" itemtype="http://schema.org/image">
                        {{ secure_asset('img/logo.png') }}
                    </div>
                    <h1><span itemprop="name">LayereDefense</span></h1>
                    <div itemprop="priceRange" itemtype="http://schema.org/priceRange">
                        $100 - $10,000
                    </div>
                    <span itemprop="description"> 
                        Technology, like Art, is a soraing exercise
                    </span>
                    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <span itemprop="streetAddress">P.O Box 100642</span><br />
                        <span itemprop="addressLocality">Arlington</span>,
                        <span itemprop="addressRegion">VA 22210</span>
                    </div>
                    Office: <span itemprop="telephone">703-688-2857</span>
                </div>

                <a href="mailto:info@layeredefense.com">info@layeredefense.com</a>
            </div>
            <div class="col s12 m8 l9 xl9">
                <div class="row hello">
                    <div class="col s12 left-align">
                    <label>Say hello!</label>
                    </div>
                </div>
                <div class="row">
                    <form class="col s12 m11 l11 xl11" id="form-contact" action="{{ secure_url('sendMail') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" name="name" type="text" class="validate" required>
                            <label for="name">Name</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="email" name="email" type="email" class="validate" required>
                            <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">                        
                            <input id="message" name="message" type="text" class="validate" required>
                            <label for="message">Message</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 right-align">
                            <button type="submit"><i class="material-icons">mail_outline</i> Send message</button>
                            </div>
                        </div>                    
                    </form>
                    <div class="col s12 m1 l1 xl1">
                        <ul class="social-list">
                            <li><a href="#!"><span class="social-icon fb"></span></a></li>
                            <li><a href="#!"><span class="social-icon tw"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="map center-align menu-scroll-3">
    <div id="map"></div>
</section>

@endsection

@section('JSextra')

@if (session('msg'))
    <script type="application/javascript"> M.toast({html: 'Message sent!!', displayLength: 4000}); </script>
@endif
<script>
  var iconBase = '{{ secure_asset('img/ping.png') }}';
  function initMap() {
    var uluru = {lat: 38.894498, lng: -77.070388};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: uluru,
      styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
    });
    var marker = new google.maps.Marker({
      position: uluru,
      map: map,
      icon: iconBase
    });
  }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBT0O5CO1TScynIvxCV8qXOjcYMni2tcPQ&callback=initMap" type="text/javascript"></script>
@endsection
