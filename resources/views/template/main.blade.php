<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <link rel="apple-touch-icon" sizes="57x57" href="{{ secure_asset('img/icon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ secure_asset('img/icon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ secure_asset('img/icon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ secure_asset('img/icon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ secure_asset('img/icon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ secure_asset('img/icon/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ secure_asset('img/icon/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ secure_asset('img/icon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ secure_asset('img/icon/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ secure_asset('img/icon/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ secure_asset('img/icon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ secure_asset('img/icon/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ secure_asset('img/icon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ secure_asset('img/icon/manifest.json') }}">
        <meta name="msapplication-TileColor" content="#441c74">
        <meta name="msapplication-TileImage" content="{{ secure_asset('img/icon/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#441c74">
        <title>LAYEREDEFENSE - @yield('title')</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.4/css/materialize.min.css" integrity="sha256-M1RAYWK/tnlEgevvMLr8tbW9WpzWS8earbGXlxgiBaI=" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" integrity="sha256-j+P6EZJVrbXgwSR5Mx+eCS6FvP9Wq27MBRC/ogVriY0=" crossorigin="anonymous" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="{{ secure_asset('css/fonts.css') }}">
        <link rel="stylesheet" href="{{ secure_asset('css/app.css') }}">
        @yield('CSSextra')
    </head>
    <body>        
        <section id="main"> 
            @yield('content')
            
            <footer class="page-footer">
              <div class="container">

                <div class="row">
                  <div class="col s12 m5 l5 xl6">
                    <img src="{{ secure_asset('img/logo.png') }}" alt="Layeredense" class="responsive-img">
                    <p class="grey-text text-lighten-4">LAYEREDEFENSE Copyright © {{ date('Y') }}</p>
                  </div>
                  <div class="col s12 m7 l7 xl6">
                    <h5 class="white-text">Explore</h5>
                    <div class="row">
                        <div class="col s12 m4 l4 xl4">
                            <ul class="border">
                                <li><a href="{{secure_url('/about')}}">About</a></li>
                                <li><a href="{{secure_url('/services')}}">Our Services</a></li>
                                <li><a href="{{secure_url('/#layeredefense-contact')}}">Contact</a></li>
                                <li><a onclick="return false" href="#!">TheHackerGround</a></li>
                            </ul>
                        </div>
                        <div class="col s12 m4 l4 xl4">
                            <ul>
                                <li><a href="{{ secure_url('/services#security-assessments') }}">Security Assessments</a></li>
                                <li><a href="{{ secure_url('/services#infrastructure') }}">Infrastructure</a></li>
                                <li><a href="{{ secure_url('/services#training') }}">Training</a></li>
                                <li><a href="{{ secure_url('/services#cloud') }}">Cloud</a></li>
                            </ul>
                        </div>
                        <div class="col s12 m4 l4 xl4">
                            <ul>
                                <li><a href="{{ secure_url('/services#open-source') }}">Open Source</a></li>
                                <li><a href="{{ secure_url('/services#incident-response') }}">Incident Response</a></li>
                            </ul>
                        </div>
                    </div>
                  </div>
                </div>

              </div>              
            </footer>
        </section>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.4/js/materialize.min.js" integrity="sha256-Jq79Dv9shjRhvRMzr71WgDr8gbZxm0AYmeJxx5jLdCU=" crossorigin="anonymous"></script>        
        <script src="https://unpkg.com/jarallax@1.9/dist/jarallax.min.js"></script>
        <script src="{{ secure_asset('js/particles.js') }}"></script>
        <script src="{{ secure_asset('js/app.js') }}"></script>
        @yield('JSextra')
    
        <!-- Header img help load -->
        <img src="{{ secure_asset('img/bg2.jpg') }}" alt="LAYEREDEFENSE" class="hide">
        <img src="{{ secure_asset('img/bg3.jpg') }}" alt="LAYEREDEFENSE" class="hide">
    </body>
</html>
