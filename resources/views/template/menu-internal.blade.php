<header class="header-in">
  <nav class="white">
    <div class="nav-wrapper container-nav">
      <a href="{{ secure_url('/') }}" class="brand-logo"><img src="{{ secure_asset('img/logo-blue.jpg') }}" alt="Layeredefense"></a>
      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down" id="general-menu">
        <li><a href="{{secure_url('/about')}}" class="{{ isset($about) ? 'active' : '' }}">About</a></li>
        <li><a href="{{secure_url('/services')}}" class="{{ isset($services) ? 'active' : '' }}">Our Services</a></li>
        <li><a href="{{secure_url('/#layeredefense-contact')}}" class="{{ isset($contact) ? 'active' : '' }}">Contact</a></li>
        <li><a onclick="return false" href="#!" class="{{ isset($hacker) ? 'active' : '' }}">TheHackerGround</a></li>
      </ul>
    </div>
  </nav>
  <ul class="sidenav" id="mobile-demo">
    <li><a href="{{secure_url('/about')}}">About</a></li>
    <li><a href="{{secure_url('/services')}}" class="active">Our Services</a></li>
    <li><a href="{{secure_url('/#layeredefense-contact')}}">Contact</a></li>
    <li><a onclick="return false" href="#!">TheHackerGround</a></li>
  </ul>
</header>