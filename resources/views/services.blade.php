@extends('template.main')

@section('title', 'Home')

@section('content')
@include('template.menu-internal',['services' => true])
@include('template.internal-title',['class' => 'services', 'title' => 'Services'])

<section class="services-list">
  <h2>
    Flexible services designed around
    <span>your business</span>
    <span class="line"></span>
  </h2>
  
  <div class="container">
    <div class="row" id="security-assessments">
      <div class="col s12 m4 l4 xl4 secure">Security Assessments</div>
      <div class="col s12 m8 l8 xl8 bg-services-purple"><p>At LayereDefense, we offer unique, thorough security assessments forged from years of direct work securing the nation's top secrets. Our approach combines multiple industry guidelines and information technology (IT) disciplines to provide our customers with a comprehensive solution. LayereDefense's ability to provide all-inclusive coverage, from enterprise policy and strategy, to technical analyses of network traffic, ensures that no aspect of your enterprise goes undetected.</p></div>
    </div>
    <div class="row" id="cloud">
      <div class="col s12 m4 l4 xl4 cloud">Cloud</div>
      <div class="col s12 m8 l8 xl8 bg-services-yellow"><p>Whether your organization is already in the cloud or planning to move services into it, our team will provide expert guidance on all aspects of cloud deployment including security, storage, computation, and cost-benefit analysis. Our specialists' knowledge includes IaaS (Infrastructure as a Service), PaaS (Platform as a Service) and SaaS (Software as a Service) solutions provided by Amazon Web Services (AWS) and Microsoft Azure. Additionally, our resources are well-versed in virtual private clouds and hybrid cloud setups.</p></div>
    </div>
    <div class="row" id="infrastructure">
      <div class="col s12 m4 l4 xl4 infra">Infrastructure</div>
      <div class="col s12 m8 l8 xl8 bg-services-aqua"><p>Whether large or small, our experts are ready to help your organization through all phases of your information technology (IT) infrastructure buildout. Our experts have years of experience in every aspect of architecting, building, and securing IT infrastructure. Our experts have architected full secure data centers from the physical aspects of power, cooling, and security, as well as the logical overall network and application services interaction. We strive to provide our customers with efficient cost effective infrastructure in all aspects.</p></div>
    </div>
    <div class="row" id="incident-response">
      <div class="col s12 m4 l4 xl4 incident">Incident Response</div>
      <div class="col s12 m8 l8 xl8 bg-services-gray"><p>Has your company recently experienced an interruption to normal operations due to viruses, malware, ransomware, or insider threat?<br /><br />LayereDefense is ready to provide your organization with a full damage assessment and remediation plan in order to get your organizational operations back to normal.</p></div>
    </div>
    <div class="row" id="training">
      <div class="col s12 m4 l4 xl4 training">Training</div>
      <div class="col s12 m8 l8 xl8 bg-services-grape"><p>Our hands-on training approach presents our customers with modern day scenarios in order to link abstract ideas and concepts into more concrete examples, ensuring knowledge acquisition and retention.</p></div>
    </div>
    <div class="row" id="open-source">
      <div class="col s12 m4 l4 xl4 os">Open Source</div>
      <div class="col s12 m8 l8 xl8 bg-services-purple-2"><p>Looking to minimize your software licensing fees and move to an open source solution?<br /><br />Let our subject matter experts provide recommendations to alternative solutions and how they can be integrated into your enterprise.</p></div>
    </div>
  </div>
  
</section>


@endsection

@section('JSextra')
@endsection
