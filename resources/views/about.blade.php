@extends('template.main')

@section('title', 'Home')

@section('content')
@include('template.menu-internal',['about' => true])
@include('template.internal-title',['class' => 'about', 'title' => 'About us'])

<section class="about-info">
  <div class="row">
    <div class="col s12 m7 l7 xl7 right-align">
      <p class="about-p line">LayereDefense is a small Veteran-owned business with the objective of making every facet of an organization's information security program more secure. LayereDefense works at multiple layers of the enterprise to ensure the failure of one layer will be actively mitigated and controlled by connected layers.</p>
      <p class="about-p">Our professionals work at all levels of an organization, as abilities range from offering strategic solutions to board members and senior executives, to providing security training to end-users.</p>
    </div>
    <div class="col s12 m5 l5 xl5">
      <p class="deformed-p">
        <span>Our team has years of experience in a wide range of technical and security disciplines such as physical security, secure programming, secure infrastructure design, and enterprise information security architecture. </span>
      </p>
    </div>
  </div>
</section>

<section class="bg-shape-1">
  <img src="{{ secure_asset('img/about-2.jpg') }}" alt="About Layeredefense" class="responsive-img">
</section>

<section class="about-working">
  <div class="row">
    <div class="col s12 m3 l2 offset-l2 xl2 offset-xl2">
      <img src="{{ secure_asset('img/about-3.jpg') }}" alt="About Person">
    </div>
    <div class="col s12 m9 l6 xl6 ">
      <p class="about-p line">
        Working with a variety of public and federal government frameworks, such as the National Institute of Standards and Technology (NIST) and Risk Management Framework (RMF), our professionals' extensive experience will assist your organization in keeping your information and assets secure.
      </p>
    </div>
  </div>  
</section>

@endsection

@section('JSextra')
@endsection
