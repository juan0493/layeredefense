require('smoothscroll-polyfill').polyfill();

const elem = document.querySelector('.parallax');
const instance = M.Parallax.init(elem);
const nav = document.querySelector('.sidenav');
const instanceNav = M.Sidenav.init(nav);


// Menu
const icons = document.getElementById('links-header');
const links = icons ? icons.querySelectorAll('a') : null;
if (links) links.forEach(function (el, index) { el.addEventListener('click', movePage) });


const contParticles = document.getElementById('particles-js');
if (contParticles && window.innerWidth>600){
    particlesJS.load('particles-js', 'js/particles.json', function () {
        console.log('Particles loaded');
    });
}

const homeBG = document.querySelector('.menu-bg');
if (homeBG) initHomeBG(homeBG)

document.addEventListener("DOMContentLoaded", function() {
    const list = document.getElementById('links-header');
    const logo = document.querySelector('.logo');
    if(logo){
        setTimeout(() => {
            logo.classList.remove('visible')
            logo.classList.add('fadeInDown')
            list.querySelectorAll('a').forEach((el, index) => {
                el.classList.remove('visible')
                el.querySelector('img').classList.add('rotateIn')
            })
        }, 800)
    }
});


function initHomeBG(list) {
    list.querySelectorAll('span').forEach((el, index)=> {
        el.addEventListener('click',changeBG)
    })
}

function changeBG(ev) {
    let current = ev.target;
    automaticChange(current);
}

function automaticChange(current) {
    const header = document.querySelector('.home-header');
    const img = document.getElementById('dynamic-img');

    homeBG.querySelectorAll('span').forEach((el, index) => el.classList.remove('active'))
    current.classList.add('active');
    img.classList.add('fadeOut');

    setTimeout(() => {
        img.src = `/img/${current.id}.jpg`;
        header.classList.remove('bg1', 'bg2', 'bg3');
        header.classList.add(current.id);
        img.classList.remove('fadeOut');
        img.classList.add('fadeIn');
    }, 300)
}

function movePage(ev) {
    ev.preventDefault();    
    let el = ev.target;
    let id = ev.srcElement.localName === 'img' ? el.parentNode.getAttribute("menu-id") : el.getAttribute("menu-id");
    document.querySelector('.' + id).scrollIntoView({ behavior: 'smooth' });
}
