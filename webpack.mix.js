let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.js('resources/assets/js/particles.js', 'public/js/particles.js')
	.copy('resources/assets/js/particles.json', 'public/js/particles.json')
   	.stylus('resources/assets/stylus/app.styl', 'public/css', {
	    use: [
	        require('rupture')(),
	        require('nib')()
	    ],
	    import: [
	        '~nib/index.styl'
	    ]
	})
	.styles([
		'resources/assets/fonts/fonts.css'
	], 'public/css/fonts.css')
	.copyDirectory('resources/assets/fonts/files','public/css/fonts')
	.disableNotifications();

