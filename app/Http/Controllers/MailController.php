<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Contact;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function send(Request $request){    	
    	Mail::to(env('MAIL_DESTINATION', 'info@layeredefense.com'))->send( new Contact($request->all()) );
    	return redirect('/#layeredefense-contact')->with('msg', 'Message sent!!');
    }
}
